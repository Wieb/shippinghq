import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

import SignIn from '@/views/VSignIn.vue'
import Dashboard from '@/views/VDashboard.vue'
import DashboardHome from '@/views/Dashboard/VHome.vue'

import Websites from '@/views/Dashboard/VWebsites.vue'
import WebsitesNew from '@/views/Dashboard/VWebsitesNew.vue'
import WebsitesEdit from '@/views/Dashboard/VWebsitesEdit.vue'

import Origins from '@/views/Dashboard/VOrigins.vue'
import OriginsNew from '@/views/Dashboard/VOriginsNew.vue'
import OriginsEdit from '@/views/Dashboard/VOriginsEdit.vue'

import Carriers from '@/views/Dashboard/VCarriers.vue'


Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'signin',
    component: SignIn,
    beforeEnter: (to, from, next) => {
      if (store.getters['auth/isAuth']) {
        next({
          name: 'dashboard'
        })
      }
      next();
    },
  },

  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    beforeEnter: (to, from, next) => {
      if (!store.getters['auth/isAuth']) {
        next({
          name: 'signin'
        })
      }
      next();
    },
    redirect: '/dashboard/home',
    children: [
      {
        path: 'home',
        name: 'home',
        component: DashboardHome
      },

      {
        path: 'websites',
        name: 'websites',
        component: Websites,
      },

      {
        path: 'websites/new',
        name: 'websites-new',
        component: WebsitesNew
      },

      {
        path: 'websites/:id/edit',
        name: 'websites-edit',
        component: WebsitesEdit
      },
  
      {
        path: 'origins',
        name: 'origins',
        component: Origins,
      },

      {
        path: 'origins/new',
        name: 'origins-new',
        component: OriginsNew
      },

      {
        path: 'origins/:id/edit',
        name: 'origins-edit',
        component: OriginsEdit
      },

      {
        path: 'carriers',
        name: 'carriers',
        component: Carriers,
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router