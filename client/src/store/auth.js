import axios from 'axios'

export default {
    namespaced: true,

    state: {
        token: null,
        user: null,
        errors: null
    },

    getters: {
        isAuth (state) {
            return state.token && state.user
        },

        user (state) {
            return state.user
        },

        authError (state) {
            return state.errors
        }
    },

    mutations: {
        SET_TOKEN (state, token) {
            state.token = token
            console.log('token set')
        },

        SET_USER (state, data) {
            state.user = data
        },

        SET_ERRORS (state, errors) {
            state.errors = errors
        }
    },

    actions: {
        async signIn ({ commit, dispatch }, credentials) {
            let response = await axios.post('signin', credentials).catch((error) => {
                if (!error.response.data || error.response.status == 401)
                    commit('SET_ERRORS', 'Incorrect email or password.')
            })

            if (response) return dispatch('attempt', response.data.token)
        },

        async attempt ({ commit, state }, token) {
            if (token) commit('SET_TOKEN', token)

            if (!state.token) return

            console.log(token)

            try {
                let response = await axios.get('me')
                commit('SET_USER', response.data)
            } catch (e) {
                commit('SET_TOKEN', null)
                commit('SET_USER', null)
            }
        },

        signOut ({ commit }) {
            return axios.post('signout').then(() => {
                commit('SET_TOKEN', null)
                commit('SET_USER', null)
            })
        }
    }
}