import axios from 'axios'

export default {
    namespaced: true,

    state: {
        errors: null,
        response: null
    },

    getters: {
        getList(state) {
            return state.list
        },

        getErrors(state) {
            return state.errors
        },

        getResponse(state) {
            return state.response
        }
    },

    mutations: {
        SET_RESPONSE(state, string) {
            state.response = string
        },

        SET_ERRORS(state, array) {
            state.errors = array
            console.log(array)
        }
    },

    actions: {
        /**
         * Get function is for getting contents
         */
        async read({ commit }, url) {
            let response = await axios.get(url).catch((error) => {
                commit('SET_ERRORS', error.response.data)
            })

            return response.data
        },

        /**
         * Post function for creating a storing a new item 
         */
        async store({ commit }, url, data) {
            let response = await axios.post(url, data).catch((error) => {
                commit('SET_ERRORS', error.response.data)
            })
            console.log(response)
        },

        /**
         * Put function is for updating contents
         */
        async update({ commit }, url, data) {
            await axios.put(url, data).catch((error) => {
                commit('SET_ERRORS', error.response.data)
                return
            })
    
            commit("SET_RESPONSE", `${data.name} was saved successfully`)
        },

        /**
         * Delete function is for deleting a item
         */
        async destroy({ commit }, url) {
            await axios.delete(url).catch((error) => {
                commit('SET_ERRORS', error.response)
                return error.response.data;
            })
        },
    },
}
