<?php

use Illuminate\Support\Facades\Route;

Route::post('signin', 'SignInController');
Route::post('signup', 'SignUpController');
Route::post('signout', 'SignOutController');

Route::group(['middleware' => ['auth.api']], function () {
    Route::get('me', 'MeController');
    Route::resource('websites', 'WebsitesController');
    Route::resource('origins', 'OriginsController');
});
