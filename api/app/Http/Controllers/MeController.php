<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MeController extends Controller
{
    public function __invoke()
    {
        $user = request()->user();
        return response()->json([
            'name' => $user->name,
            'email' => $user->email,
        ]);
    }
}
