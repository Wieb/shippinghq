<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\SignUpRequest;

class SignUpController extends Controller
{
    public function __invoke()
    {
        $validator = Validator::make(request()->all(), [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(), 200);
        }

        $user = User::create([
            'name' => $validator->valid()['name'],
            'email' => $validator->valid()['email'],
            'password' => Hash::make($validator->valid()['password'])
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('token'), 201);
    }
}
