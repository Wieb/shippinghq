<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Website;

class Origin extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function websites()
    {
        return $this->belongsToMany(Website::class);
    }
}
