<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Website;
use App\Origin;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

        /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Return all Websites with a relationship to this User.
     *
     * @return array
     */
    public function websites()
    {
        return $this->hasMany(Website::class);
    }

    /**
     * Return all Websites with a relationship to this User.
     *
     * @return array
     */
    public function origins()
    {
        return $this->hasMany(Origin::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            $user->websites()->create([
                'name' => 'ShipperHQ Test Store',
                'url' => 'http://www.mytestsite.com',
                'description' => 'This website...',
            ]);

            $user->origins()->create([
                'name' => 'California Test',
                'websites' => ['ShipperHQ Test Store'],

                'country' => 'United States',
                'street_address' => '1 Broadway',
                'city' => 'Los Angeles',
                'state' => 'California',
                'Zip' => '90034'
            ]);
        });
    }
}
